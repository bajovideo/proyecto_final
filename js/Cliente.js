import { database } from './app.js';
import { ref, get } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js';

const productosRef = ref(database, 'Productos');
const cardsContainer = document.querySelector('.cards-container');
const fragranceTypeSelect = document.getElementById('fragranceType'); // Obtén el select del HTML

async function fetchDataAndPopulateCards() {
  try {
    const snapshot = await get(productosRef);

    if (snapshot.exists()) {
      const productosData = snapshot.val();

      for (const key in productosData) {
        const productData = productosData[key];
        const cardDiv = createCardElement(productData);

        // Agrega el tipo de fragancia al select
        addFragranceTypeToSelect(productData.tipodefrag);

        cardsContainer.appendChild(cardDiv);
      }
    } else {
      console.log("The 'Productos' node does not contain data.");
    }
  } catch (error) {
    console.error('Error retrieving and populating data:', error);
  }
}

function createCardElement(data) {
  const cardDiv = document.createElement('div');
  cardDiv.classList.add('card');
  cardDiv.setAttribute('data-fragrance-type', data.tipodefrag); // Establece el tipo de fragancia como atributo

  // Customize the structure as needed
  cardDiv.innerHTML = `
    <img src="${data.url}" class="card-img-top" alt="Product Image">
    <div class="card-body">
      <h5 class="card-title">${data.nombre}</h5>
      <p class="card-text">
        <strong>Cantidad:</strong> ${data.cantidad}<br>
        <strong>Descripción:</strong> ${data.descripcion}<br>
        <strong>Status:</strong> ${data.status}<br>
        <strong>Tipo de Fragancia:</strong> ${data.tipodefrag}<br>
      </p>
      <h4 class="card-text">$${data.precio}</h4>
    </div>
  `;

  return cardDiv;
}

function addFragranceTypeToSelect(type) {
  // Verifica si el tipo de fragancia ya está en el select
  if (fragranceTypeSelect.querySelector(`[value="${type}"]`) === null) {
    const option = document.createElement('option');
    option.value = type;
    option.textContent = type;
    fragranceTypeSelect.appendChild(option);
  }
}

window.addEventListener('load', fetchDataAndPopulateCards);

// Agrega un event listener al botón de búsqueda
document.getElementById('searchButton').addEventListener('click', () => {
  const selectedFragranceType = fragranceTypeSelect.value;

  // Obtén todos los productos
  const productCards = document.querySelectorAll('.card');

  // Itera sobre los productos y muéstralos o ocúltalos según el tipo de fragancia seleccionado
  productCards.forEach((card) => {
    const cardType = card.getAttribute('data-fragrance-type');
    if (selectedFragranceType === 'todos' || selectedFragranceType === cardType) {
      card.style.display = 'block';
    } else {
      card.style.display = 'none';
    }
  });
});

