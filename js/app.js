import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getAuth} from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js";
import { getStorage} from "https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js";
// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBANTZn8wqF-l8kMiL4FfD5o81sMRz1nRA",
  authDomain: "proyectofinal-9f69d.firebaseapp.com",
  projectId: "proyectofinal-9f69d",
  storageBucket: "proyectofinal-9f69d.appspot.com",
  messagingSenderId: "49148888739",
  appId: "1:49148888739:web:da55ec0f22762dfb19e116"
};

// Initialize Firebase
  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  export {app}
  export const auth = getAuth(app);
  export const storage = getStorage(app)