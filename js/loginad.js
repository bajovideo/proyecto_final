// login.js
import { getAuth, signInWithEmailAndPassword } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js';

const emailInput = document.getElementById('Email');
const passwordInput = document.getElementById('Password');
const loginButton = document.querySelector('#btnEnviar');
const errorMessage = document.getElementById('error-message');

console.log(loginButton);
loginButton.addEventListener('click', async (e) => {
  e.preventDefault(); // Prevenir el comportamiento predeterminado del formulario (recargar la página)

  const email = emailInput.value;
  const password = passwordInput.value;

  const auth = getAuth(); // Obtener la instancia de autenticación de Firebase

  try {
    await signInWithEmailAndPassword(auth, email, password);
    // Inicio de sesión exitoso, redirigir a otra página.
    window.location.href = '/html/administrador.html';
  } catch (error) {
    let errorMessage = "Llene todos los campos o contraseña incorrecta.";
    if (error.code === 'auth/wrong-password') {
      // La contraseña es incorrecta, muestra una alerta
      errorMessage = 'La contraseña es incorrecta. Intente nuevamente.';
    } else {
      // Otro error de inicio de sesión
      errorMessage = error.message;
    }
    alert(errorMessage);
  }
});